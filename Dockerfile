FROM python:buster

WORKDIR /app

COPY requirements.txt .
RUN python -m pip install -r requirements.txt

COPY . .
ENTRYPOINT ["python", "-u", "hs_bot.py"]
