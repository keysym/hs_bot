import discord
from discord.ext import commands

class About(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def invite(self, ctx):
        await ctx.send('**Invite: ** <https://discord.com/oauth2/authorize?client_id=570072939530027009&permissions=384064&scope=bot>')

    @commands.command()
    async def git(self, ctx):
        await ctx.send('**Git: ** https://gitlab.com/Hinigatsu/hs_bot')

    @commands.command()
    async def help(self, ctx):
        # Basic implementation of a help command
        # Consider HelpCommand?
        await ctx.send('**Basic Hearthstone card query:**\n```[[card_name]]```')

def setup(bot):
    bot.add_cog(About(bot))
