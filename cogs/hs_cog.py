import re, os, asyncio
from enum import Enum

import aiohttp
import discord
from discord.ext import commands

class Locales(Enum):
    de_DE = 'de_DE'
    en_US = 'en_US'
    es_ES = 'es_ES'
    es_MX = 'es_MX'
    fr_FR = 'fr_FR'
    it_IT = 'it_IT'
    ja_JP = 'ja_JP'
    ko_KR = 'ko_KR'
    pl_PL = 'pl_PL'
    pt_BR = 'pt_BR'
    ru_RU = 'ru_RU'
    th_TH = 'th_TH'
    zh_CN = 'zh_CN'
    zh_TW = 'zh_TW'

class Hs_cog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.locale = Locales.pt_BR.value
        self.card_regex = re.compile(r'\[\[.*\]\]')

    @commands.command(aliases=['locale'])
    @commands.is_owner()
    async def change_locale(self, ctx, locale_arg):
        try:
            self.locale = Locales(locale_arg).value
        except ValueError:
            return await ctx.send(f'*{locale_arg}* is not a valid locale.')

        await ctx.send(f'hs_cog locale changed to *{self.locale}*')

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author == self.bot.user:
            return

        if not (regex_matching_obj := self.card_regex.search(message.content)):
            return

        # Removes "[" and "]" from query
        card_name = re.sub('\[|\]', '', regex_matching_obj.group())

        # TODO: Make a better query.
        # Uses "card_name" to query a card. It acts as a textFilter. 
        # Only the first card on the response is passed to get the ID and then gets displayed.
        async with self.bot.session.get('https://us.api.blizzard.com/hearthstone/cards', params = {'locale': self.locale, 'textFilter': card_name, 'access_token': self.blizz_api_token}) as res:
            res.raise_for_status()
            res_as_json = await res.json()

        e = discord.Embed()
        e.set_image(url=res_as_json['cards'][0]['image'])

        await message.channel.send(embed=e)

    async def get_access_token(self, client_id, client_secret):
        async with self.bot.session.post('https://us.battle.net/oauth/token', data = {'grant_type': 'client_credentials', 'client_id': client_id, 'client_secret': client_secret}) as res:
            self.blizz_api_token =  (await res.json())['access_token']

def setup(bot):
    loop = asyncio.get_event_loop()

    hs_cog = Hs_cog(bot)
    loop.create_task(hs_cog.get_access_token(os.environ['BLIZZ_CLIENT_ID'], os.environ['BLIZZ_CLIENT_SECRET']))

    bot.add_cog(hs_cog)

