import os, asyncio

import aiohttp
import discord
from discord.ext import commands

class Hs_bot(commands.Bot):
    def __init__(self, command_prefix='!hs '):
        super().__init__(command_prefix, help_command=None)

        self.loop = asyncio.get_event_loop()
        # FIXIT: DeprecationWarning
        self.session = aiohttp.ClientSession(loop=self.loop)

    async def on_ready(self):
        print('It works!')

    async def close(self):
        await self.session.close()

if __name__ == '__main__':
    hs_bot = Hs_bot()
    hs_bot.load_extension('cogs.about')
    hs_bot.load_extension('cogs.hs_cog')

    hs_bot.run(os.environ['DISCORD_BOT_TOKEN'])

